FROM golang:1.21.5-alpine AS build

WORKDIR /go/src/don-vito

RUN addgroup -S -g 1001 don-vito && adduser -S -D -H -G don-vito -u 1001 don-vito

COPY go.mod go.sum ./
COPY vendor vendor

COPY cmd cmd
COPY pkg pkg

RUN go mod verify
RUN go build -o /go/bin -v -ldflags="-s -w" ./cmd/don-vito-updater


FROM alpine:3.19.0

COPY --from=build /etc/passwd /etc/group /etc/
COPY --from=build /go/bin/ /usr/local/bin

USER don-vito
