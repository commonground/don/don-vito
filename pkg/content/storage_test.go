package content_test

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"

	"gitlab.com/commonground/don/don-vito/pkg/content"
	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

func setupTestGitRepository(t *testing.T) string {
	t.Helper()

	dir := t.TempDir()
	r, err := git.PlainInit(dir, false)
	assert.NoError(t, err)

	// Set initial branch to main
	head := plumbing.NewSymbolicReference(plumbing.HEAD, plumbing.NewBranchReferenceName("main"))
	err = r.Storer.SetReference(head)
	assert.NoError(t, err)

	wt, _ := r.Worktree()
	f, _ := wt.Filesystem.Create("content/repository/test-repository.yaml")

	in := []byte(`organization:
  name: Test organization
  ooid: 42
repositories:
  - github: organization-a/repository-1
  - gitlab: organization-b/repository-2
  - gitlab: organization-b/group-c/repository-3
    apis:
      - test-api
`)

	_, err = f.Write(in)
	assert.NoError(t, err)

	_, err = wt.Add(".")
	assert.NoError(t, err)

	_, err = wt.Commit("Init", &git.CommitOptions{
		All:    true,
		Author: &object.Signature{Name: "Test", When: time.Date(2023, 10, 6, 7, 42, 12, 0, time.UTC)},
	})
	assert.NoError(t, err)

	return dir
}

func TestExtractRepositoryFiles(t *testing.T) {
	baseDir := setupTestGitRepository(t)
	workingDir := t.TempDir()

	opts := &content.GitOptions{
		CloneURL:   fmt.Sprintf("file://%s", baseDir),
		BaseBranch: "main",
	}
	s, err := content.NewStorage(workingDir, opts)
	if !assert.NoError(t, err) {
		assert.FailNow(t, "NewStorage failed")
	}

	files, err := s.ExtractRepositoryFiles()
	require.NoError(t, err)
	assert.Len(t, files, 1)
	assert.True(t, time.Date(2023, 10, 6, 7, 42, 12, 0, time.UTC).Equal(files[0].LastModified))
}

func TestStorageSaveFile(t *testing.T) {
	baseDir := setupTestGitRepository(t)
	workingDir := t.TempDir()

	opts := &content.GitOptions{
		CloneURL:    fmt.Sprintf("file://%s", baseDir),
		BaseBranch:  "main",
		AuthorName:  "Test author",
		AuthorEmail: "test@example.com",
	}
	s, err := content.NewStorage(workingDir, opts)
	require.NoError(t, err)

	files, err := s.ExtractRepositoryFiles()
	require.NoError(t, err)

	file := files[0]
	file.Repositories.Add(content.Repository{Type: platform.GitHub, FullPath: "test/test"})

	hash, err := s.SaveRepositoryFile(file)
	assert.NoError(t, err)
	assert.False(t, hash.IsZero())

	// Inspect repository
	r, err := git.PlainOpen(workingDir)
	assert.NoError(t, err)

	head, _ := r.Head()
	c, _ := r.CommitObject(head.Hash())

	assert.Equal(t, "Test author", c.Author.Name)
	assert.Equal(t, "test@example.com", c.Author.Email)
	assert.Equal(t, "Update repository: test-repository", c.Message)
}

func TestStorageSaveSmallerFile(t *testing.T) {
	baseDir := setupTestGitRepository(t)
	workingDir := t.TempDir()

	opts := &content.GitOptions{
		CloneURL:    fmt.Sprintf("file://%s", baseDir),
		BaseBranch:  "main",
		AuthorName:  "Test author",
		AuthorEmail: "test@example.com",
	}
	s, err := content.NewStorage(workingDir, opts)
	if !assert.NoError(t, err) {
		assert.FailNow(t, "NewStorage failed")
	}

	files, _ := s.ExtractRepositoryFiles()

	file := files[0]
	file.Repositories.Remove(platform.GitHub, "organization-a/repository-1")
	file.Repositories.Remove(platform.GitLab, "organization-b/group-c/repository-3")

	_, _ = s.SaveRepositoryFile(file)

	f, _ := os.Open(filepath.Join(workingDir, "content", "repository", "test-repository.yaml"))
	actual, _ := io.ReadAll(f)
	expected := `
organization:
  name: Test organization
  ooid: 42
repositories:
  - gitlab: organization-b/repository-2
`

	assert.Equal(t, expected[1:], string(actual))
}
