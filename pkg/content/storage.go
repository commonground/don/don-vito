package content

import (
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/storer"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"gopkg.in/yaml.v3"
)

type GitOptions struct {
	CloneURL    string
	BaseBranch  string
	AccessToken string
	AuthorName  string
	AuthorEmail string
}

type Storage struct {
	workingDir string
	opts       *GitOptions

	repository *git.Repository
	worktree   *git.Worktree
	baseRef    *plumbing.Reference

	transportAuth transport.AuthMethod
}

func NewStorage(workingDir string, opts *GitOptions) (s *Storage, err error) {
	s = &Storage{
		workingDir: filepath.Clean(workingDir),
		opts:       opts,
		transportAuth: &http.BasicAuth{
			Password: opts.AccessToken,
		},
	}
	err = s.init()

	return
}

func (s *Storage) init() (err error) {
	BaseBranch := plumbing.NewBranchReferenceName(s.opts.BaseBranch)
	s.repository, err = checkoutGitRepository(s.workingDir, s.opts.CloneURL, s.transportAuth, BaseBranch)
	if err != nil {
		return fmt.Errorf("checkout git repository: %w", err)
	}

	s.worktree, _ = s.repository.Worktree() // We know it's not a bare clone
	s.baseRef, _ = s.repository.Head()

	// Clean all existing branches
	// unclear why we clean branches
	iter, _ := s.repository.Branches()
	err = iter.ForEach(func(r *plumbing.Reference) error {
		if r.Name() == BaseBranch {
			return nil
		}
		return s.repository.Storer.RemoveReference(r.Name())
	})
	if err != nil {
		return fmt.Errorf("clean all branches: %w", err)
	}

	return
}

func (s *Storage) ExtractRepositoryFiles() (files []*RepositoryFile, err error) {
	repositoryPath := filepath.Join("content", "repository")
	root := filepath.Join(s.workingDir, repositoryPath)

	err = filepath.WalkDir(root, func(path string, entry fs.DirEntry, _ error) error {
		if entry.IsDir() {
			return nil
		}

		name := entry.Name()

		if filepath.Ext(name) != ".yaml" {
			return nil
		}

		r, err := os.Open(path)
		if err != nil {
			return err
		}

		file, err := ReadRepositoryFile(r, filepath.Join(repositoryPath, name))
		if err != nil {
			return fmt.Errorf("Reading file %s: %w", name, err)
		}

		iter, err := s.repository.Log(&git.LogOptions{
			FileName: &file.Path,
			Order:    git.LogOrderCommitterTime,
		})
		if err != nil {
			return fmt.Errorf("Commit history for file %s: %w", name, err)
		}

		c, err := iter.Next()
		if err != nil {
			return fmt.Errorf("Commit info for file %s: %w", name, err)
		}

		if c != nil {
			file.LastModified = c.Committer.When
		}

		if err != nil {
			return fmt.Errorf("Reading file info %s: %w", name, err)
		}

		files = append(files, file)

		return nil
	})

	return
}

func (s Storage) SaveRepositoryFile(file *RepositoryFile) (hash plumbing.Hash, err error) {
	filePath := filepath.Join(s.workingDir, file.Path)
	f, err := os.Create(filePath)
	if err != nil {
		return
	}

	if err = WriteRepositoryFile(file, f); err != nil {
		return
	}

	if len(file.Repositories.items) == 0 && file.IgnoredRepositories == nil {
		log.Printf("Organization with no repositories. Deleting file %s", filePath)
		os.Remove(filePath)
	}

	if st, _ := s.worktree.Status(); st.IsClean() {
		return
	}

	if _, err = s.worktree.Add(file.Path); err != nil {
		return
	}

	if hash, err = s.worktree.Commit(fmt.Sprintf("Update repository: %s", file.Name()), &git.CommitOptions{
		Author: s.newAuthor(),
	}); err != nil {
		return
	}

	return hash, err
}

func (s Storage) newAuthor() *object.Signature {
	return &object.Signature{
		Name:  s.opts.AuthorName,
		Email: s.opts.AuthorEmail,
		When:  time.Now(),
	}
}

func (s *Storage) CreateBranch(branchName string) (branch plumbing.ReferenceName, err error) {
	if err = s.worktree.Reset(&git.ResetOptions{Mode: git.HardReset}); err != nil {
		return
	}

	branch = plumbing.NewBranchReferenceName(branchName)
	err = s.worktree.Checkout(&git.CheckoutOptions{
		Branch: branch,
		Hash:   s.baseRef.Hash(),
		Create: true,
	})

	return
}

func (s *Storage) SwitchToBranch(name string) (branch plumbing.ReferenceName, err error) {
	branch = plumbing.NewBranchReferenceName(name)
	err = s.worktree.Checkout(&git.CheckoutOptions{
		Branch: branch,
		Create: false,
		Force:  true,
	})

	return
}

func (s *Storage) PushBranch(branch plumbing.ReferenceName) (err error) {
	err = s.repository.Push(&git.PushOptions{
		Auth: s.transportAuth,
		RefSpecs: []config.RefSpec{
			config.RefSpec(fmt.Sprintf("%s:%s", branch, branch)),
		},
		Force: true,
	})

	return
}

func ReadRepositoryFile(r io.Reader, path string) (*RepositoryFile, error) {
	f := &RepositoryFile{
		Path: path,
	}

	dec := yaml.NewDecoder(r)
	if err := dec.Decode(f); err != nil {
		return nil, err
	}

	if f.IgnoredRepositories == nil {
		f.IgnoredRepositories = NewRepositories()
	}

	return f, nil
}

func WriteRepositoryFile(file *RepositoryFile, w io.Writer) (err error) {
	if len(file.IgnoredRepositories.Items()) == 0 {
		file.IgnoredRepositories = nil
	}

	enc := yaml.NewEncoder(w)
	enc.SetIndent(2)
	if err = enc.Encode(file); err != nil {
		return
	}

	return enc.Close()
}

func checkoutGitRepository(path, url string, auth transport.AuthMethod, branch plumbing.ReferenceName) (*git.Repository, error) {
	r, err := git.PlainClone(path, false, &git.CloneOptions{
		URL:           url,
		Auth:          auth,
		ReferenceName: branch,
		Progress:      nil,
		Tags:          git.NoTags,
	})

	if err == nil {
		return r, nil
	} else if !errors.Is(err, git.ErrRepositoryAlreadyExists) {
		return nil, err
	}

	r, err = git.PlainOpen(path)
	if err != nil {
		return nil, err
	}

	err = r.Fetch(&git.FetchOptions{
		RemoteName: git.DefaultRemoteName,
		Tags:       git.NoTags,
		Auth:       auth,
	})
	if err != nil && !errors.Is(err, git.NoErrAlreadyUpToDate) {
		return nil, err
	}

	remoteBranch := plumbing.NewRemoteReferenceName(git.DefaultRemoteName, branch.Short())
	ref, err := storer.ResolveReference(r.Storer, remoteBranch)
	if err != nil {
		return nil, err
	}

	wt, _ := r.Worktree()
	err = wt.Checkout(&git.CheckoutOptions{Branch: branch, Force: true})
	if err != nil {
		return nil, err
	}

	err = wt.Reset(&git.ResetOptions{Commit: ref.Hash(), Mode: git.HardReset})

	return r, err
}
