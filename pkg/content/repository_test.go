package content_test

import (
	"bytes"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"

	"gitlab.com/commonground/don/don-vito/pkg/content"
	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

func TestRepositoryFile(t *testing.T) {
	in := `organization:
  name: Test organization
  ooid: 42
repositories:
  - github: organization-a/repository-1
  - gitlab: organization-b/repository-2
  - gitlab: organization-b/group-c/repository-3
    apis:
      - test-api
`

	file, err := content.ReadRepositoryFile(strings.NewReader(in), "test/test-repository.yaml")
	require.NoError(t, err)

	assert.Equal(t, "test-repository", file.Name())
	assert.Equal(t, "Test organization", file.Organization.Name)
	assert.Equal(t, 42, file.Organization.OOID)

	items := file.Repositories.Items()
	assert.Len(t, items, 3)
	assert.Equal(t, "github:organization-a/repository-1", items[0].String())
	assert.Equal(t, "gitlab:organization-b/repository-2", items[1].String())
	assert.Equal(t, "gitlab:organization-b/group-c/repository-3", items[2].String())
	assert.Equal(t, "test-api", items[2].APIs[0])

	assert.Len(t, file.IgnoredRepositories.Items(), 0)
}

func TestRepositoryFileRoundtrip(t *testing.T) {
	in := `organization:
  name: Test organization
  ooid: 42
repositories:
  - github: organization-a/repository-1
  - gitlab: organization-b/repository-2
  - gitlab: organization-b/group-c/repository-3
    apis:
      - test-api
`

	file, err := content.ReadRepositoryFile(strings.NewReader(in), "test/test-repository.yaml")
	assert.NoError(t, err)

	out := bytes.Buffer{}
	err = content.WriteRepositoryFile(file, &out)
	assert.NoError(t, err)

	assert.Equal(t, in, out.String())
}

func TestRepositoryMarshal(t *testing.T) {
	_, err := yaml.Marshal(&content.Repository{})
	assert.ErrorIs(t, err, content.ErrUnknownRepositoryType)

	_, err = yaml.Marshal(&content.Repository{Type: 3})
	assert.ErrorIs(t, err, content.ErrUnknownRepositoryType)
}

func TestRepositoryUnmarshal(t *testing.T) {
	r := &content.Repository{}

	err := yaml.Unmarshal([]byte(`unknown: "test"`), &r)
	assert.ErrorIs(t, err, content.ErrUnknownRepositoryType)

	err = yaml.Unmarshal([]byte("_broken"), r)
	assert.Error(t, err)
	assert.NotErrorIs(t, err, content.ErrUnknownRepositoryType)
}

func TestRepositoriesUnmarshal(t *testing.T) {
	rs := content.Repositories{}

	err := yaml.Unmarshal([]byte("_broken"), &rs)
	assert.Error(t, err)
	assert.Equal(t, 0, rs.Len())
}

func TestRepositoriesRemove(t *testing.T) {
	rs := content.NewRepositories(
		content.Repository{Type: platform.GitLab, FullPath: "organization-1/repository-a"},
		content.Repository{Type: platform.GitHub, FullPath: "organization-2/repository-b"},
		content.Repository{Type: platform.GitLab, FullPath: "organization-3/repository-c"},
		content.Repository{Type: platform.GitLab, FullPath: "organization-4/repository-d"},
	)

	rs.Remove(platform.GitHub, "non/existing")
	assert.Equal(t, 4, rs.Len())

	rs.Remove(platform.GitHub, "organization-2/repository-b")
	assert.Equal(t, 3, rs.Len())

	exists := rs.MergeFromPlatform(platform.Repository{
		Type:     platform.GitLab,
		FullPath: "organization-4/repository-d",
	})
	assert.True(t, exists)

	items := rs.Items()
	assert.Equal(t, "organization-1/repository-a", items[0].FullPath)
	assert.Equal(t, "organization-4/repository-d", items[1].FullPath)
	assert.Equal(t, "organization-3/repository-c", items[2].FullPath)
}

func TestRepositoriesContains(t *testing.T) {
	rs := content.NewRepositories(
		content.Repository{
			Type:     platform.GitLab,
			FullPath: "organization-1/repository-a",
			APIs:     []string{"test-api-1", "test-api-2"},
		},
	)

	assert.True(t, rs.Contains(platform.GitLab, "organization-1/repository-a"))
	assert.False(t, rs.Contains(platform.GitLab, "organization-1"))
	assert.False(t, rs.Contains(platform.GitLab, ""))
	assert.False(t, rs.Contains(platform.GitHub, "organization-2/repository-b"))
	assert.False(t, rs.Contains(42, "organization-2/repository-b"))
}

func TestRepositoriesMergeFromPlatform(t *testing.T) {
	rs := content.NewRepositories(
		content.Repository{
			Type:     platform.GitHub,
			FullPath: "organization-1/repository-a",
		},
		content.Repository{
			Type:     platform.GitLab,
			FullPath: "organization-2/repository-b",
			APIs:     []string{"test-api-1", "test-api-2"},
		},
	)

	exists := rs.MergeFromPlatform(platform.Repository{
		Type:     platform.GitHub,
		FullPath: "organization-1/repository-a",
	})
	assert.True(t, exists)

	exists = rs.MergeFromPlatform(platform.Repository{
		Type:     platform.GitLab,
		FullPath: "organization-2/repository-b",
	})
	assert.True(t, exists)

	exists = rs.MergeFromPlatform(platform.Repository{
		Type:     platform.GitHub,
		FullPath: "organization-3/repository-c",
	})
	assert.False(t, exists)

	expected := []*content.Repository{
		{
			Type:     platform.GitHub,
			FullPath: "organization-1/repository-a",
		},
		{
			Type:     platform.GitLab,
			FullPath: "organization-2/repository-b",
			APIs:     []string{"test-api-1", "test-api-2"},
		},
		{
			Type:     platform.GitHub,
			FullPath: "organization-3/repository-c",
		},
	}

	assert.Equal(t, expected, rs.Items())
}

func TestRepositoriesSort(t *testing.T) {
	rs := content.NewRepositories(
		content.Repository{Type: platform.GitLab, FullPath: "organization-2/repository-b"},
		content.Repository{Type: platform.GitHub, FullPath: "organization-1/repository-a"},
	)

	rs.Add(content.Repository{Type: platform.GitHub, FullPath: "zz-organization/repository"})
	rs.Add(content.Repository{Type: platform.GitLab, FullPath: "aa-organization/repository"})

	rs.Sort()

	rs.MergeFromPlatform(platform.Repository{Type: platform.GitLab, FullPath: "aa-organization/repository"})
	rs.MergeFromPlatform(platform.Repository{Type: platform.GitHub, FullPath: "zz-organization/repository"})

	expected := []*content.Repository{
		{Type: platform.GitLab, FullPath: "aa-organization/repository"},
		{Type: platform.GitHub, FullPath: "organization-1/repository-a"},
		{Type: platform.GitLab, FullPath: "organization-2/repository-b"},
		{Type: platform.GitHub, FullPath: "zz-organization/repository"},
	}

	assert.Equal(t, expected, rs.Items())
}

func TestDistinctOwners(t *testing.T) {
	rs := []*content.Repository{
		{Type: platform.GitHub, FullPath: "organization-1/repository"},
		{Type: platform.GitHub, FullPath: "organization-1/repository"},
		{Type: platform.GitLab, FullPath: "organization-2/repository"},
		{Type: platform.GitLab, FullPath: "organization-2/repository"},
		{Type: platform.GitLab, FullPath: "organization-2/repository"},
		{Type: platform.GitLab, FullPath: "organization-3/repository"},
		{Type: platform.GitLab, FullPath: "organization-4/group-1/repository"},
		{Type: platform.GitLab, FullPath: "organization-4/group-1/repository"},
		{Type: platform.GitLab, FullPath: "organization-4/group-2/repository"},
		{Type: platform.Unknown, FullPath: "organization-5/repository"},
	}

	owners := content.DistinctOwners(rs)
	assert.Len(t, owners, 2)

	assert.Len(t, owners[platform.GitHub], 1)
	assert.Equal(t, "organization-1", owners[platform.GitHub][0])

	assert.Len(t, owners[platform.GitLab], 3)
	assert.Equal(t, "organization-2", owners[platform.GitLab][0])
	assert.Equal(t, "organization-3", owners[platform.GitLab][1])
	assert.Equal(t, "organization-4", owners[platform.GitLab][2])
}

func TestSplitFullPath(t *testing.T) {
	tests := []struct {
		path              string
		owner, repository string
	}{
		{"organization/repository", "organization", "repository"},
		{"organization/group/repository", "organization", "group/repository"},
		{"organization/group-1/group-2/repository", "organization", "group-1/group-2/repository"},

		{"/repository", "", "repository"},
		{"organization/", "organization", ""},
		{"repository", "repository", ""},
		{"", "", ""},
	}
	for _, tc := range tests {
		t.Run("_"+tc.path, func(t *testing.T) {
			owner, repository := content.SplitFullPath(tc.path)
			assert.Equal(t, tc.owner, owner)
			assert.Equal(t, tc.repository, repository)
		})
	}
}
