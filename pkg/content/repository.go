package content

import (
	"errors"
	"fmt"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"gopkg.in/yaml.v3"

	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

const pathSeperator = "/"

var ErrUnknownRepositoryType = errors.New("Unknown repository type")

type RepositoryFile struct {
	Path         string    `yaml:"-"`
	LastModified time.Time `yaml:"-"`

	Organization        Organization  `yaml:"organization"`
	Repositories        *Repositories `yaml:"repositories"`
	IgnoredRepositories *Repositories `yaml:"ignored_repositories,omitempty"`
}

func (f *RepositoryFile) Name() string {
	fileName := filepath.Base(f.Path)
	return fileName[:len(fileName)-len(filepath.Ext(fileName))]
}

type Organization struct {
	Name string `yaml:"name"`
	OOID int    `yaml:"ooid"`
}

type Repository struct {
	Type     platform.RepositoryType
	FullPath string
	APIs     []string
}

type repositoryYAML struct {
	GitHub string   `yaml:"github,omitempty"`
	GitLab string   `yaml:"gitlab,omitempty"`
	APIs   []string `yaml:"apis,omitempty"`
}

func (r Repository) MarshalYAML() (interface{}, error) {
	data := repositoryYAML{
		APIs: r.APIs,
	}

	switch r.Type {
	case platform.GitHub:
		data.GitHub = r.FullPath
	case platform.GitLab:
		data.GitLab = r.FullPath
	default:
		return nil, ErrUnknownRepositoryType
	}

	return data, nil
}

func (r *Repository) UnmarshalYAML(value *yaml.Node) error {
	var data repositoryYAML
	if err := value.Decode(&data); err != nil {
		return err
	}

	switch {
	case data.GitHub != "":
		r.FullPath = data.GitHub
		r.Type = platform.GitHub
	case data.GitLab != "":
		r.FullPath = data.GitLab
		r.Type = platform.GitLab
	default:
		return ErrUnknownRepositoryType
	}

	r.APIs = data.APIs

	return nil
}

func (r Repository) String() string {
	return strings.ToLower(fmt.Sprintf("%s:%s", r.Type, r.FullPath))
}

type Repositories struct {
	items []*Repository
	index map[string]int
}

func NewRepositories(items ...Repository) (rs *Repositories) {
	rs = &Repositories{
		index: make(map[string]int, len(items)),
	}

	for _, item := range items {
		rs.Add(item)
	}

	return
}

func (rs *Repositories) Add(r Repository) {
	key := r.String()
	if _, ok := rs.index[key]; !ok {
		rs.index[key] = len(rs.items)
	}

	rs.items = append(rs.items, &r)
}

func (rs *Repositories) Remove(t platform.RepositoryType, path string) {
	key := strings.ToLower(fmt.Sprintf("%s:%s", t, path))

	if i, exists := rs.index[key]; exists {
		last := len(rs.items) - 1

		rs.items[i] = rs.items[last]
		rs.index[rs.items[i].String()] = i

		rs.items = rs.items[:last]
		delete(rs.index, key)
	}
}

func (rs *Repositories) Contains(t platform.RepositoryType, path string) bool {
	key := strings.ToLower(fmt.Sprintf("%s:%s", t, path))
	_, ok := rs.index[key]
	return ok
}

func (rs Repositories) Items() []*Repository {
	return rs.items
}

func (rs *Repositories) MergeFromPlatform(p platform.Repository) (exists bool) {
	key := p.String()

	var (
		r *Repository
		i int
	)
	if i, exists = rs.index[key]; exists {
		r = rs.items[i]
	} else {
		r = &Repository{Type: p.Type}
		rs.index[key] = len(rs.items)
		rs.items = append(rs.items, r)
	}

	r.FullPath = p.FullPath

	return
}

func (rs *Repositories) Sort() {
	sort.Sort(rs)

	for i, r := range rs.items {
		rs.index[r.String()] = i
	}
}

func (rs Repositories) Len() int { return len(rs.items) }
func (rs Repositories) Less(i, j int) bool {
	return strings.ToLower(rs.items[i].FullPath) < strings.ToLower(rs.items[j].FullPath)
}
func (rs Repositories) Swap(i, j int) { rs.items[i], rs.items[j] = rs.items[j], rs.items[i] }

func (rs Repositories) MarshalYAML() (interface{}, error) {
	return rs.items, nil
}

func (rs *Repositories) UnmarshalYAML(value *yaml.Node) error {
	var items []Repository

	if err := value.Decode(&items); err != nil {
		return err
	}

	rs.index = make(map[string]int, len(items))

	for _, item := range items {
		rs.Add(item)
	}

	return nil
}

func DistinctOwners(rs []*Repository) platform.Owners {
	seen := map[platform.RepositoryType]map[string]bool{
		platform.GitHub: {},
		platform.GitLab: {},
	}
	owners := platform.Owners{}

	for _, r := range rs {
		tSeen, ok := seen[r.Type]
		if !ok {
			continue
		}

		owner, _ := SplitFullPath(r.FullPath)
		if _, ok := tSeen[owner]; !ok {
			tSeen[owner] = true
			owners[r.Type] = append(owners[r.Type], owner)
		}
	}

	return owners
}

func SplitFullPath(path string) (owner, rest string) {
	owner, rest, _ = strings.Cut(path, pathSeperator)
	return
}
