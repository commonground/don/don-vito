package gitlab

import "github.com/xanzy/go-gitlab"

type Project struct {
	p *gitlab.Project
	u *gitlab.User
}

func (c Project) CloneURL() string {
	return c.p.HTTPURLToRepo
}

func (c Project) DefaultBranch() string {
	return c.p.DefaultBranch
}

func (c Project) GitAuthor() (name, email string) {
	return c.u.Name, c.u.Email
}

func GetProject(c *gitlab.Client, name string) (*Project, error) {
	project, _, err := c.Projects.GetProject(name, &gitlab.GetProjectOptions{})
	if err != nil {
		return nil, err
	}

	user, _, err := c.Users.CurrentUser()
	if err != nil {
		return nil, err
	}

	return &Project{p: project, u: user}, err
}
