package gitlab

import (
	"bytes"
	"fmt"
	"regexp"
	"sort"
	"strings"
	"text/template"

	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

type MergeRequestState string

const (
	Opened MergeRequestState = "opened"
	Closed MergeRequestState = "closed"
)

type MergeRequest struct {
	ID           int
	ProjectID    int
	Description  string
	State        MergeRequestState
	SourceBranch string

	Repositories MergeRequestRepositories
}

type MergeRequestRepositories []MergeRequestRepository

func (m MergeRequestRepositories) Sort() {
	sort.Sort(m)
}

func (m MergeRequestRepositories) Len() int { return len(m) }
func (m MergeRequestRepositories) Less(i, j int) bool {
	return strings.ToLower(m[i].FullName) < strings.ToLower(m[j].FullName)
}
func (m MergeRequestRepositories) Swap(i, j int) { m[i], m[j] = m[j], m[i] }

type MergeRequestRepository struct {
	Checked  bool
	Type     platform.RepositoryType
	FullName string
	WebURL   string
}

func (r MergeRequestRepository) String() string {
	return strings.ToLower(fmt.Sprintf("%s:%s", r.Type, r.FullName))
}

type descriptionData struct {
	Added   MergeRequestRepositories
	Deleted MergeRequestRepositories
}

const (
	startMarker    = "<!-- repositories:start -->"
	startMarkerLen = 27
	endMarker      = "<!-- repositories:end -->"
	checkedChar    = "x"

	descriptionTmplText = `
{{- if .Added -}}
### New repositories ({{ .Added | len }})

_Note: to ignore a repository, uncheck the checkbox.
Repositories that are ~~strikethrough~~ were processed and are ignored._

` + startMarker + `
{{- range .Added }}
- [{{ .Checked | check }}] {{ if not .Checked }}~~{{ end }}{{ .Type }}: [{{ .FullName }}]({{ .WebURL }}){{ if not .Checked }}~~{{ end }}
{{- end }}
` + endMarker + `

---
{{- end }}
{{- if .Deleted }}

### Deleted repositories ({{ .Deleted | len }})

{{- range .Deleted }}
- {{ .Type }}: {{ .FullName }}
{{- end }}
---
{{- end }}

This is an automatically generated MR by [Don Vito](https://gitlab.com/commonground/don/don-vito)`
)

var (
	descriptionTmpl = template.Must(template.New("description").
			Funcs(template.FuncMap{
			"check": func(checked bool) string {
				if checked {
					return checkedChar
				}
				return " "
			},
		}).
		Parse(descriptionTmplText))
	repositoryRegex = regexp.MustCompile(`^- \[([ |x])\] ~{0,2}(\S*): \[(\S*)\]\((\S*)\)~{0,2}$`)
)

func GenerateMRDescription(added, deleted MergeRequestRepositories) (string, error) {
	var buf bytes.Buffer
	data := descriptionData{
		Added:   added,
		Deleted: deleted,
	}

	if err := descriptionTmpl.Execute(&buf, data); err != nil {
		return "", err
	}

	return buf.String(), nil
}

func ParseMRDescription(s string) MergeRequestRepositories {
	markerPos := strings.Index(s, startMarker)
	if markerPos == -1 {
		return nil
	}

	start := markerPos + startMarkerLen

	markerPos = strings.Index(s[start:], endMarker)
	if markerPos == -1 {
		return nil
	}

	mrs := MergeRequestRepositories{}
	end := start + markerPos

	lines := strings.Split(s[start:end], "\n")
	for _, line := range lines {
		line = strings.Trim(line, " ")
		if line == "" {
			continue
		}

		matches := repositoryRegex.FindStringSubmatch(line)
		if len(matches) != 5 {
			continue
		}

		mr := MergeRequestRepository{
			Checked:  matches[1] == checkedChar,
			Type:     platform.RepositoryTypeFromString(matches[2]),
			FullName: matches[3],
			WebURL:   matches[4],
		}

		mrs = append(mrs, mr)
	}

	return mrs
}
