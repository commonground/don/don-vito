package gitlab_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/don/don-vito/pkg/gitlab"
	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

func TestParseMergeRequstDescriptionMarker(t *testing.T) {
	mrr := gitlab.ParseMRDescription("Test")
	assert.Nil(t, mrr)

	mrr = gitlab.ParseMRDescription("TestTest<!-- repositories:start -->TestTest")
	assert.Nil(t, mrr)

	mrr = gitlab.ParseMRDescription("TestTest<!-- repositories:start -->TestTest<!-- repositories:end -->TestTest")
	assert.Len(t, mrr, 0)
}

func TestMergeRequstDescriptionRoundtrip(t *testing.T) {
	added := gitlab.MergeRequestRepositories{
		{
			Checked:  false,
			Type:     platform.GitHub,
			FullName: "organization-a/repository-1",
			WebURL:   "https://example.com/1",
		},
		{
			Checked:  false,
			Type:     platform.GitLab,
			FullName: "organization-b/repository-2",
			WebURL:   "https://example.com/2",
		},
		{
			Checked:  true,
			Type:     platform.GitLab,
			FullName: "organization-b/repository-3",
			WebURL:   "https://example.com/3",
		},
	}

	deleted := gitlab.MergeRequestRepositories{
		{Type: platform.GitHub, FullName: "organization-e/repository-4"},
		{Type: platform.GitLab, FullName: "organization-f/repository-5"},
	}

	d, err := gitlab.GenerateMRDescription(added, deleted)
	assert.NoError(t, err)
	assert.NotEmpty(t, d)

	actual := gitlab.ParseMRDescription(d)
	assert.Equal(t, added, actual)
}

func TestMergeRequstDescription(t *testing.T) {
	added := gitlab.MergeRequestRepositories{
		{
			Checked:  true,
			Type:     platform.GitHub,
			FullName: "organization-a/repository-1",
			WebURL:   "https://example.com/1",
		},
		{
			Checked:  false,
			Type:     platform.GitLab,
			FullName: "organization-b/repository-2",
			WebURL:   "https://example.com/2",
		},
	}

	deleted := gitlab.MergeRequestRepositories{
		{Type: platform.GitHub, FullName: "organization-c/repository-3"},
		{Type: platform.GitLab, FullName: "organization-d/repository-4"},
	}

	d, err := gitlab.GenerateMRDescription(added, deleted)

	assert.NoError(t, err)
	assert.Contains(t, d, "New repositories (2)")
	assert.Contains(t, d, "- [x] GitHub: [organization-a/repository-1](https://example.com/1)")
	assert.Contains(t, d, "- [ ] ~~GitLab: [organization-b/repository-2](https://example.com/2)~~")

	assert.Contains(t, d, "Deleted repositories (2)")
	assert.Contains(t, d, "- GitHub: organization-c/repository-3")
	assert.Contains(t, d, "- GitLab: organization-d/repository-4")
}

func TestMergeRequestRepositoriesSort(t *testing.T) {
	actual := gitlab.MergeRequestRepositories{
		{Checked: true, Type: platform.GitHub, FullName: "b"},
		{Checked: false, Type: platform.GitLab, FullName: "d"},
		{Checked: true, Type: platform.GitHub, FullName: "C"},
		{Checked: false, Type: platform.GitLab, FullName: "A"},
	}

	actual.Sort()

	expected := gitlab.MergeRequestRepositories{
		{Checked: false, Type: platform.GitLab, FullName: "A"},
		{Checked: true, Type: platform.GitHub, FullName: "b"},
		{Checked: true, Type: platform.GitHub, FullName: "C"},
		{Checked: false, Type: platform.GitLab, FullName: "d"},
	}

	assert.Equal(t, expected, actual)
}
