package platform

import (
	"fmt"
	"strings"
)

const (
	Unknown RepositoryType = iota
	GitHub
	GitLab
)

type RepositoryType int

func (t RepositoryType) String() string {
	switch t {
	case GitHub:
		return "GitHub"
	case GitLab:
		return "GitLab"
	}
	return "Unknown"
}

func RepositoryTypeFromString(s string) RepositoryType {
	switch s {
	case "GitHub", "github":
		return GitHub
	case "GitLab", "gitlab":
		return GitLab
	}
	return Unknown
}

type Repository struct {
	Type     RepositoryType
	FullPath string
	WebURL   string
	IsFork   bool
}

func (s Repository) String() string {
	return strings.ToLower(fmt.Sprintf("%s:%s", s.Type, s.FullPath))
}
