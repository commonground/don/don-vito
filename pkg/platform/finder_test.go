package platform_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

func TestFindRepository(t *testing.T) {
	m := mock.Mock{}

	m.On("github", "owner-a").
		Return([]*platform.Repository{{Type: platform.GitHub, FullPath: "owner-a/path-1"}}, nil)
	m.On("github", "owner-b").
		Return([]*platform.Repository{{Type: platform.GitHub, FullPath: "owner-b/path-1"}}, nil)

	m.On("gitlab", "owner-c").
		Return([]*platform.Repository{{Type: platform.GitLab, FullPath: "owner-c/path-3"}}, nil)

	finder := platform.RepositoryFinder{
		Funcs: map[platform.RepositoryType]platform.ListFunc{
			platform.GitHub: func(owner string) ([]*platform.Repository, error) {
				args := m.MethodCalled("github", owner)
				return args.Get(0).([]*platform.Repository), args.Error(1)
			},
			platform.GitLab: func(owner string) ([]*platform.Repository, error) {
				args := m.MethodCalled("gitlab", owner)
				return args.Get(0).([]*platform.Repository), args.Error(1)
			},
		},
	}

	owners := map[platform.RepositoryType][]string{
		platform.GitHub: {"owner-a", "owner-b"},
		platform.GitLab: {"owner-c"},
	}
	repos, err := finder.Find(owners)

	assert.NoError(t, err)
	assert.Len(t, repos, 3)

	m.AssertExpectations(t)
}

func TestFindUnknowRepository(t *testing.T) {
	finder := platform.RepositoryFinder{}

	owners := map[platform.RepositoryType][]string{
		platform.GitHub: {"owner-a"},
	}
	repos, err := finder.Find(owners)

	assert.Nil(t, repos)
	assert.ErrorIs(t, err, platform.ErrUnsupportedRepositoryType)
}

func TestFindRepositoryError(t *testing.T) {
	testErr := errors.New("test") //nolint:goerr113  // It is a test error

	m := mock.Mock{}
	m.On("gitlab", "owner")

	finder := platform.RepositoryFinder{
		Funcs: map[platform.RepositoryType]platform.ListFunc{
			platform.GitLab: func(owner string) ([]*platform.Repository, error) {
				m.MethodCalled("gitlab", owner)
				return nil, testErr
			},
		},
	}

	owners := map[platform.RepositoryType][]string{platform.GitLab: {"owner"}}
	repos, err := finder.Find(owners)

	assert.Nil(t, repos)
	assert.ErrorIs(t, err, testErr)

	m.AssertExpectations(t)
}
