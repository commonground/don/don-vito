package platform_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

func TestRepositoryTypeString(t *testing.T) {
	assert.EqualValues(t, "GitHub", platform.GitHub.String())
	assert.EqualValues(t, "GitLab", platform.GitLab.String())
	assert.EqualValues(t, "Unknown", platform.Unknown.String())
}

func TestRepositoryTypeFromString(t *testing.T) {
	tests := []struct {
		in       string
		expected platform.RepositoryType
	}{
		{"GitHub", platform.GitHub},
		{"github", platform.GitHub},
		{"GitLab", platform.GitLab},
		{"gitlab", platform.GitLab},
		{"other", platform.Unknown},
		{"", platform.Unknown},
	}

	for _, tc := range tests {
		t.Run(tc.in, func(t *testing.T) {
			assert.Equal(t, tc.expected, platform.RepositoryTypeFromString(tc.in))
		})
	}
}

func TestRepositoryString(t *testing.T) {
	assert.Equal(t, "github:organization/repository", platform.Repository{Type: platform.GitHub, FullPath: "organization/repository"}.String())
	assert.Equal(t, "gitlab:group/sub-group/project", platform.Repository{Type: platform.GitLab, FullPath: "group/sub-group/project"}.String())
}
