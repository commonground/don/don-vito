package platform

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"github.com/google/go-github/v57/github"
	"github.com/xanzy/go-gitlab"
)

var ErrUnsupportedRepositoryType = errors.New("Unsupported repository type")

type Owners map[RepositoryType][]string

type ListFunc func(owner string) ([]*Repository, error)

type RepositoryFinder struct {
	Funcs map[RepositoryType]ListFunc
}

func NewRepositoryFinder(ghClient *github.Client, glClient *gitlab.Client) *RepositoryFinder {
	return &RepositoryFinder{
		Funcs: map[RepositoryType]ListFunc{
			GitHub: githubListFunc(ghClient),
			GitLab: gitlabListFunc(glClient),
		},
	}
}

func (u *RepositoryFinder) Find(ownersMap Owners) ([]*Repository, error) {
	repos := []*Repository{}

	for t, owners := range ownersMap {
		list := u.Funcs[t]
		if list == nil {
			return nil, ErrUnsupportedRepositoryType
		}

		for _, owner := range owners {
			r, err := list(owner)
			if err != nil {
				return nil, fmt.Errorf("listing repositories for owner '%s': %w", owner, err)
			}
			repos = append(repos, r...)
		}
	}

	return repos, nil
}

const githubSpecialRepository = ".github"

func githubListFunc(client *github.Client) ListFunc {
	return func(owner string) ([]*Repository, error) {
		opts := github.RepositoryListByOrgOptions{
			Type:      "all",
			Sort:      "full_name",
			Direction: "asc",

			ListOptions: github.ListOptions{
				PerPage: 100,
			},
		}

		repos := []*Repository{}
		ctx := context.Background()

		for {
			githubRepos, resp, err := client.Repositories.ListByOrg(ctx, owner, &opts)
			if err != nil {
				if resp != nil && resp.StatusCode == http.StatusNotFound {
					break
				}
				return nil, err
			}

			for _, repo := range githubRepos {
				if repo.GetPrivate() ||
					repo.GetSize() == 0 ||
					repo.GetName() == githubSpecialRepository {
					continue
				}

				repos = append(repos, &Repository{
					Type:     GitHub,
					FullPath: repo.GetFullName(),
					WebURL:   repo.GetHTMLURL(),
					IsFork:   repo.GetFork(),
				})
			}

			if resp.NextPage == 0 {
				break
			}

			opts.Page = resp.NextPage
		}

		return repos, nil
	}
}

func gitlabListFunc(client *gitlab.Client) ListFunc {
	return func(owner string) ([]*Repository, error) {
		opts := gitlab.ListGroupProjectsOptions{
			Visibility:       gitlab.Ptr(gitlab.PublicVisibility),
			IncludeSubGroups: gitlab.Ptr(true),
			Simple:           gitlab.Ptr(false),
			OrderBy:          gitlab.Ptr("path"),
			Sort:             gitlab.Ptr("asc"),

			ListOptions: gitlab.ListOptions{
				PerPage: 100,
			},
		}

		repos := []*Repository{}

		for {
			projects, resp, err := client.Groups.ListGroupProjects(owner, &opts)
			if err != nil {
				if resp != nil && resp.StatusCode == http.StatusNotFound {
					break
				}
				return nil, err
			}

			for _, p := range projects {
				if p.EmptyRepo || p.MarkedForDeletionAt != nil {
					continue
				}

				repos = append(repos, &Repository{
					Type:     GitLab,
					FullPath: p.PathWithNamespace,
					WebURL:   p.WebURL,
					IsFork:   p.ForkedFromProject != nil,
				})
			}

			if resp.NextPage == 0 {
				break
			}

			opts.Page = resp.NextPage
		}

		return repos, nil
	}
}
