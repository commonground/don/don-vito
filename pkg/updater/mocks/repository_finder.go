// Code generated by mockery v2.15.0. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	platform "gitlab.com/commonground/don/don-vito/pkg/platform"
)

// RepositoryFinder is an autogenerated mock type for the RepositoryFinder type
type RepositoryFinder struct {
	mock.Mock
}

type RepositoryFinder_Expecter struct {
	mock *mock.Mock
}

func (_m *RepositoryFinder) EXPECT() *RepositoryFinder_Expecter {
	return &RepositoryFinder_Expecter{mock: &_m.Mock}
}

// Find provides a mock function with given fields: owners
func (_m *RepositoryFinder) Find(owners platform.Owners) ([]*platform.Repository, error) {
	ret := _m.Called(owners)

	var r0 []*platform.Repository
	if rf, ok := ret.Get(0).(func(platform.Owners) []*platform.Repository); ok {
		r0 = rf(owners)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*platform.Repository)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(platform.Owners) error); ok {
		r1 = rf(owners)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// RepositoryFinder_Find_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Find'
type RepositoryFinder_Find_Call struct {
	*mock.Call
}

// Find is a helper method to define mock.On call
//   - owners platform.Owners
func (_e *RepositoryFinder_Expecter) Find(owners interface{}) *RepositoryFinder_Find_Call {
	return &RepositoryFinder_Find_Call{Call: _e.mock.On("Find", owners)}
}

func (_c *RepositoryFinder_Find_Call) Run(run func(owners platform.Owners)) *RepositoryFinder_Find_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(platform.Owners))
	})
	return _c
}

func (_c *RepositoryFinder_Find_Call) Return(_a0 []*platform.Repository, _a1 error) *RepositoryFinder_Find_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

type mockConstructorTestingTNewRepositoryFinder interface {
	mock.TestingT
	Cleanup(func())
}

// NewRepositoryFinder creates a new instance of RepositoryFinder. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewRepositoryFinder(t mockConstructorTestingTNewRepositoryFinder) *RepositoryFinder {
	mock := &RepositoryFinder{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
