package updater_test

import (
	"bytes"
	"errors"
	"log"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"

	"github.com/go-git/go-git/v5/plumbing"

	"gitlab.com/commonground/don/don-vito/pkg/updater/mocks"

	"gitlab.com/commonground/don/don-vito/pkg/content"
	"gitlab.com/commonground/don/don-vito/pkg/gitlab"
	"gitlab.com/commonground/don/don-vito/pkg/platform"
	"gitlab.com/commonground/don/don-vito/pkg/updater"
)

var errTest = errors.New("test")

func TestEmptyRepository(t *testing.T) {
	u, s, _ := newUpdater(t)

	s.EXPECT().ExtractRepositoryFiles().Return([]*content.RepositoryFile{}, nil)

	err := u.Update()
	assert.NoError(t, err)
}

type TestUpdaterSuite struct {
	suite.Suite

	updater *updater.Updater
	storage *mocks.Storage_Expecter
	finder  *mocks.RepositoryFinder_Expecter

	testFile content.RepositoryFile

	logBuffer bytes.Buffer
}

func (ts *TestUpdaterSuite) SetupSuite() {
	log.SetOutput(&ts.logBuffer)
	log.SetFlags(0)
}

func (ts *TestUpdaterSuite) SetupTest() {
	t := ts.T()

	u, s, f := newUpdater(t)

	ts.updater = u
	ts.storage = s.EXPECT()
	ts.finder = f.EXPECT()

	ts.testFile = content.RepositoryFile{
		Path:         "test-organization.yaml",
		LastModified: time.Date(2023, 10, 6, 7, 42, 12, 0, time.UTC),
		Repositories: content.NewRepositories(
			content.Repository{Type: platform.GitLab, FullPath: "organization-1/repository-a"},
			content.Repository{Type: platform.GitLab, FullPath: "organization-1/repository-b"},
		),
		IgnoredRepositories: content.NewRepositories(),
	}
	ts.storage.ExtractRepositoryFiles().Return([]*content.RepositoryFile{&ts.testFile}, nil)

	repos := []*platform.Repository{
		{Type: platform.GitLab, FullPath: "organization-1/repository-a"},
		{Type: platform.GitLab, FullPath: "organization-1/repository-b"},
		{Type: platform.GitLab, FullPath: "organization-2/repository-c"},
	}
	ts.finder.Find(mock.Anything).Return(repos, nil).Once()

	ts.logBuffer.Reset()
}

func (ts *TestUpdaterSuite) Test_FileOrdering() {
	ts.storage.ExtractRepositoryFiles().Unset()
	ts.storage.ExtractRepositoryFiles().Return([]*content.RepositoryFile{
		{
			Path:         "second",
			LastModified: time.Date(2023, 10, 6, 7, 42, 12, 0, time.UTC),
			Repositories: content.NewRepositories(
				content.Repository{Type: platform.GitLab, FullPath: "second/repository"},
			),
		},
		{
			Path:         "first",
			LastModified: time.Date(2023, 10, 6, 7, 42, 11, 0, time.UTC),
			Repositories: content.NewRepositories(
				content.Repository{Type: platform.GitLab, FullPath: "first/repository"},
			),
		},
	}, nil)

	ts.finder.Find(mock.Anything).Unset()

	first := ts.finder.Find(platform.Owners{platform.GitLab: []string{"first"}}).Return(nil, errTest)

	ts.finder.Find(platform.Owners{platform.GitLab: []string{"second"}}).
		Return(nil, errTest).
		NotBefore(first.Call)

	err := ts.updater.Update()
	ts.NoError(err)
}

func (ts *TestUpdaterSuite) Test_NoNewRepositories() {
	ts.testFile.Repositories.Add(
		content.Repository{Type: platform.GitLab, FullPath: "organization-2/repository-c"},
	)

	owners := platform.Owners{platform.GitLab: []string{"organization-1", "organization-2"}}
	repos := []*platform.Repository{
		{Type: platform.GitLab, FullPath: "organization-1/repository-a"},
		{Type: platform.GitLab, FullPath: "organization-1/repository-b"},
		{Type: platform.GitLab, FullPath: "organization-2/repository-c"},
	}

	ts.finder.Find(owners).Return(repos, nil)

	err := ts.updater.Update()
	ts.NoError(err)

	output := ts.logBuffer.String()
	expected := `Repository files found: 1
Processing file: test-organization
Found repositories: 3
No updates found
`
	ts.Equal(expected, output)
}

func (ts *TestUpdaterSuite) Test_ExistingMergeRequest_NoUpdates() {
	ts.storage.SaveRepositoryFile(&ts.testFile).Return(plumbing.Hash{}, nil)

	err := ts.updater.Update()
	ts.Require().NoError(err)

	output := ts.logBuffer.String()
	ts.Contains(output, "File is up-to-date")
}

func TestUpdater(t *testing.T) {
	suite.Run(t, &TestUpdaterSuite{})
}

func newUpdater(t *testing.T) (*updater.Updater, *mocks.Storage, *mocks.RepositoryFinder) {
	t.Helper()

	s := mocks.NewStorage(t)
	f := mocks.NewRepositoryFinder(t)

	return updater.NewUpdater(s, f), s, f
}

func TestUpdateRepositoryFile(t *testing.T) {
	file := content.RepositoryFile{
		Repositories: content.NewRepositories(
			content.Repository{Type: platform.GitHub, FullPath: "organization-1/repository-a"},
			content.Repository{Type: platform.GitHub, FullPath: "organization-1/repository-b"},
		),
		IgnoredRepositories: content.NewRepositories(),
	}

	prs := []*platform.Repository{
		{Type: platform.GitHub, FullPath: "organization-1/repository-a"},
		{Type: platform.GitHub, FullPath: "organization-1/repository-b"},
		{Type: platform.GitHub, FullPath: "organization-2/repository-c"},
		{Type: platform.GitLab, FullPath: "organization-3/repository-e"},
	}

	mrs := gitlab.MergeRequestRepositories{
		{Checked: false, Type: platform.GitLab, FullName: "organization-3/repository-e"},
	}

	actual, deleted := updater.UpdateRepositoryFile(&file, prs, mrs)
	expected := gitlab.MergeRequestRepositories{
		{Checked: true, Type: platform.GitHub, FullName: "organization-2/repository-c"},
		{Checked: false, Type: platform.GitLab, FullName: "organization-3/repository-e"},
	}
	assert.Equal(t, expected, actual)
	assert.Empty(t, deleted)

	assert.Equal(t, 3, file.Repositories.Len())
	assert.Equal(t, 1, file.IgnoredRepositories.Len())
	assert.Equal(t,
		&content.Repository{Type: platform.GitLab, FullPath: "organization-3/repository-e"},
		file.IgnoredRepositories.Items()[0],
	)
}

func TestUpdateRepositoryFileIgnored(t *testing.T) {
	file := content.RepositoryFile{
		Repositories: content.NewRepositories(
			content.Repository{Type: platform.GitHub, FullPath: "organization-1/repository-a"},
		),
		IgnoredRepositories: content.NewRepositories(
			content.Repository{Type: platform.GitHub, FullPath: "organization-1/repository-b"},
		),
	}

	prs := []*platform.Repository{
		{Type: platform.GitHub, FullPath: "organization-1/repository-a"},
		{Type: platform.GitHub, FullPath: "organization-1/repository-b"},
	}

	added, deleted := updater.UpdateRepositoryFile(&file, prs, gitlab.MergeRequestRepositories{})
	assert.Empty(t, added)
	assert.Empty(t, deleted)
}

func TestUpdateRepositoryFileForked(t *testing.T) {
	file := content.RepositoryFile{
		Repositories: content.NewRepositories(
			content.Repository{Type: platform.GitLab, FullPath: "organization-1/repository-a"},
			content.Repository{Type: platform.GitLab, FullPath: "organization-1/repository-b"},
		),
		IgnoredRepositories: content.NewRepositories(),
	}

	prs := []*platform.Repository{
		{Type: platform.GitLab, FullPath: "organization-1/repository-a", IsFork: false},
		{Type: platform.GitLab, FullPath: "organization-1/repository-b", IsFork: true},
		{Type: platform.GitLab, FullPath: "organization-1/repository-c", IsFork: true},
	}

	added, deleted := updater.UpdateRepositoryFile(&file, prs, gitlab.MergeRequestRepositories{})
	assert.Empty(t, added)
	assert.Empty(t, deleted)
}

func TestUpdateRepositoryFileDeleted(t *testing.T) {
	file := content.RepositoryFile{
		Repositories: content.NewRepositories(
			content.Repository{Type: platform.GitLab, FullPath: "organization-1/repository-a"},
			content.Repository{Type: platform.GitLab, FullPath: "organization-2/repository-c"},
		),
		IgnoredRepositories: content.NewRepositories(
			content.Repository{Type: platform.GitHub, FullPath: "organization-1/repository-b"},
			content.Repository{Type: platform.GitHub, FullPath: "organization-2/repository-d"},
		),
	}

	prs := []*platform.Repository{
		{Type: platform.GitLab, FullPath: "organization-1/repository-a", IsFork: true},
		{Type: platform.GitHub, FullPath: "organization-1/repository-b", IsFork: false},
	}

	added, deleted := updater.UpdateRepositoryFile(&file, prs, gitlab.MergeRequestRepositories{})
	expected := gitlab.MergeRequestRepositories{
		{Type: platform.GitLab, FullName: "organization-2/repository-c"},
		{Type: platform.GitHub, FullName: "organization-2/repository-d"},
	}

	assert.Len(t, added, 0)
	assert.Len(t, deleted, 2)
	assert.Equal(t, expected, deleted)

	assert.Equal(t, 1, file.Repositories.Len())
	assert.Equal(t, 1, file.IgnoredRepositories.Len())
}
