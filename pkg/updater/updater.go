package updater

//go:generate mockery --all --case underscore --with-expecter

import (
	"fmt"
	"log"
	"sort"

	"github.com/go-git/go-git/v5/plumbing"

	"gitlab.com/commonground/don/don-vito/pkg/content"
	"gitlab.com/commonground/don/don-vito/pkg/gitlab"
	"gitlab.com/commonground/don/don-vito/pkg/platform"
)

type Storage interface {
	ExtractRepositoryFiles() ([]*content.RepositoryFile, error)
	SaveRepositoryFile(file *content.RepositoryFile) (plumbing.Hash, error)

	SwitchToBranch(name string) (plumbing.ReferenceName, error)
	PushBranch(branch plumbing.ReferenceName) error
}

type RepositoryFinder interface {
	Find(owners platform.Owners) ([]*platform.Repository, error)
}

type Updater struct {
	storage Storage
	finder  RepositoryFinder
}

func NewUpdater(s Storage, f RepositoryFinder) *Updater {
	return &Updater{
		storage: s,
		finder:  f,
	}
}

func (u *Updater) Update() (err error) {
	files, err := u.storage.ExtractRepositoryFiles()
	if err != nil {
		return fmt.Errorf("Error extracting the repository files: %w", err)
	}

	log.Printf("Repository files found: %d", len(files))

	// Sort files, oldest first
	sort.Slice(files, func(i, j int) bool {
		return files[i].LastModified.Before(files[j].LastModified)
	})

	for _, file := range files {
		log.Printf("Processing file: %s", file.Name())

		owners := content.DistinctOwners(file.Repositories.Items())
		repositories, err := u.finder.Find(owners)
		if err != nil {
			log.Printf("Failed to find repositories: %v", err)
			continue
		}

		log.Printf("Found repositories: %d", len(repositories))

		if err = u.updateFile(file, repositories); err != nil {
			log.Printf("Failed to update file: %v", err)
			continue
		}
	}

	return
}

func (u *Updater) updateFile(file *content.RepositoryFile, repositories []*platform.Repository) (err error) {
	added := gitlab.MergeRequestRepositories{}

	added, deleted := UpdateRepositoryFile(file, repositories, added)
	addedLen := added.Len()
	deletedLen := deleted.Len()

	if addedLen == 0 && deletedLen == 0 {
		log.Println("No updates found")
		return
	}

	log.Printf("New: %d, deleted: %d", addedLen, deletedLen)

	file.Repositories.Sort()
	file.IgnoredRepositories.Sort()

	commitHash, err := u.storage.SaveRepositoryFile(file)
	if err != nil {
		return fmt.Errorf("save file: %w", err)
	}

	if !commitHash.IsZero() {
		log.Println("File updated")
	} else {
		log.Println("File is up-to-date")
	}

	return
}

func UpdateRepositoryFile(file *content.RepositoryFile, prs []*platform.Repository, mrs gitlab.MergeRequestRepositories) (added, deleted gitlab.MergeRequestRepositories) {
	toIgnore := map[string]bool{}

	for _, mr := range mrs {
		if !mr.Checked {
			toIgnore[mr.String()] = true
		}
	}

	added = make(gitlab.MergeRequestRepositories, 0, len(prs))
	deleted = make(gitlab.MergeRequestRepositories, 0, len(prs))

	prsIndex := map[string]bool{}

	for _, r := range prs {
		key := r.String()

		prsIndex[key] = true

		if r.IsFork || file.IgnoredRepositories.Contains(r.Type, r.FullPath) {
			continue
		}

		var ignore, exists bool

		if _, ignore = toIgnore[key]; ignore {
			exists = file.IgnoredRepositories.MergeFromPlatform(*r)
		} else {
			exists = file.Repositories.MergeFromPlatform(*r)
		}

		if exists {
			continue
		}

		mrr := gitlab.MergeRequestRepository{
			Checked:  !ignore,
			Type:     r.Type,
			FullName: r.FullPath,
			WebURL:   r.WebURL,
		}

		added = append(added, mrr)
	}

	for _, r := range file.Repositories.Items() {
		if _, exists := prsIndex[r.String()]; !exists {
			file.Repositories.Remove(r.Type, r.FullPath)
			deleted = append(deleted, gitlab.MergeRequestRepository{Type: r.Type, FullName: r.FullPath})
		}
	}

	for _, r := range file.IgnoredRepositories.Items() {
		if _, exists := prsIndex[r.String()]; !exists {
			file.IgnoredRepositories.Remove(r.Type, r.FullPath)
			deleted = append(deleted, gitlab.MergeRequestRepository{Type: r.Type, FullName: r.FullPath})
		}
	}

	added.Sort()
	deleted.Sort()

	return added, deleted
}
