package main

import (
	"errors"
	"flag"
	fs "io/fs"
	"log"
	"os"

	"github.com/google/go-github/v57/github"
	"github.com/joho/godotenv"
	gitlabext "github.com/xanzy/go-gitlab"

	"gitlab.com/commonground/don/don-vito/pkg/content"
	"gitlab.com/commonground/don/don-vito/pkg/gitlab"
	"gitlab.com/commonground/don/don-vito/pkg/platform"
	"gitlab.com/commonground/don/don-vito/pkg/updater"
)

type options struct {
	workingDir    string
	gitlabProject string
	gitlabToken   string
	githubToken   string
	BaseBranch    string
}

func loadEnvFile() {
	err := godotenv.Load(".env")
	var fileNotFound *fs.PathError
	if errors.As(err, &fileNotFound) {
		// .env file is not required
		log.Println("No .env file being used")
	} else if err != nil {
		log.Fatalf("Error while reading file: %s", err)
	}
}

func parseFlags() *options {
	loadEnvFile()
	o := &options{}
	flag.StringVar(&o.workingDir, "working-dir", envValue("WORKING_DIR"), "Working directory of content files")
	flag.StringVar(&o.gitlabProject, "gitlab-project", envValue("GITLAB_PROJECT"), "GitLab project to update")
	flag.StringVar(&o.gitlabToken, "gitlab-token", envValue("GITLAB_TOKEN"), "GitLab access token")
	flag.StringVar(&o.githubToken, "github-token", envValue("GITHUB_TOKEN"), "GitHub access token")
	flag.StringVar(&o.BaseBranch, "base-branch", envValue("BASE_BRANCH"), "Branch to pull and push")

	flag.Parse()

	if o.gitlabToken == "" {
		log.Fatalln("A GitLab access token is required")
	}

	return o
}

func main() {
	o := parseFlags()
	os.Exit(run(o))
}

const (
	exitSuccess = 0
	exitError   = 1
)

func run(o *options) int {
	if o.workingDir == "" {
		tempDir, err := os.MkdirTemp("", "don-vito-*")
		if err != nil {
			log.Fatalf("Creating temp working directory: %v\n", err)
			return exitError
		}
		defer os.RemoveAll(tempDir)
		o.workingDir = tempDir
	}

	gitlabClient, err := gitlabext.NewClient(o.gitlabToken)
	if err != nil {
		log.Printf("Error creating a new GitLab client: %v", err)
		return exitError
	}

	project, err := gitlab.GetProject(gitlabClient, o.gitlabProject)
	if err != nil {
		log.Printf("Error getting the GitLab project information: %v", err)
		return exitError
	}

	BaseBranch := o.BaseBranch
	if BaseBranch == "" {
		BaseBranch = project.DefaultBranch()
	}

	opts := &content.GitOptions{
		CloneURL:    project.CloneURL(),
		BaseBranch:  BaseBranch,
		AccessToken: o.gitlabToken,
	}

	opts.AuthorName, opts.AuthorEmail = project.GitAuthor()

	storage, err := content.NewStorage(o.workingDir, opts)
	if err != nil {
		log.Printf("Error creating new storage: %v", err)
		return exitError
	}

	log.Printf("Switching to branch: %s", BaseBranch)
	branch, err := storage.SwitchToBranch(BaseBranch)
	if err != nil {
		log.Printf("Error switching to branch: %v", err)
		return exitError
	}

	var githubClient *github.Client

	if o.githubToken != "" {
		githubClient = github.NewClient(nil).WithAuthToken(o.githubToken)
	} else {
		githubClient = github.NewClient(nil)
	}

	finder := platform.NewRepositoryFinder(githubClient, gitlabClient)

	u := updater.NewUpdater(storage, finder)
	if err = u.Update(); err != nil {
		log.Printf("Error updating files: %v", err)
		return exitError
	}

	log.Println("Pushing branch")
	if err = storage.PushBranch(branch); err != nil {
		log.Printf("Push branch: %v", err)
	}

	return exitSuccess
}

func envValue(key string) string {
	value := os.Getenv(key)
	return value
}
