# Don Vito

Finds git repositories from government organisations and updates the 
[developer.ovherheid.nl content
repository](https://gitlab.com/commonground/don/don-content) with the latest
list of repositories. 

## How it works

1. Clones the content repository
2. In the repository files of the content repository, it gets all gitlab and
   github organisation accounts.
3. It uses the gitlab & github API's to get the list of repositories linked to
   each organisation account.
4. Updates the local files of the content repository so that the list of
   repositories linked to an organisation matches the information from gitlab &
   github.
5. Pushes the changes to the main branch of the content repository

## Requirements

- [Go](https://go.dev/)

## Setup

1. Create an `.env` file

```sh
cp .env.example .env
```

1. Set environment variables in `.env` file. 
  - For `GITLAB_TOKEN` you'll have to [create a gitlab access
    token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token).
  - For `GITHUB_TOKEN` you'll have to [create a github access
    token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#creating-a-personal-access-token-classic).

## Run

```sh
go run ./cmd/don-vito-updater
```

## Build

```sh
go build ./cmd/don-vito-updater
```

## Add new packages

1. Add package to `go.mod` and `go.sum` and download package source into module
   cache:

```sh
go get <package_url>
```

2. Add package to vendor directory:

```sh
go mod vendor
```

## Hosting/ cronjob

This application is being triggered by a [Pipeline schedule](https://gitlab.com/commonground/don/don-content/-/pipeline_schedules) within Gitlab filed under our [`don-content`](https://gitlab.com/commonground/don/don-content) project. More information about cronjobs within this project can be found over [here](https://gitlab.com/groups/commonground/don/-/wikis/Cronjobs).

## Licence

Copyright © VNG Realisatie 2022

[Licensed under the EUPLv1.2](LICENCE.md)
